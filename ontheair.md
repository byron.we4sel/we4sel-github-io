# ON THE AIR
### ++[Home](index.md) ++[Station Info](station.md)++[On The Air](ontheair.md)++[Online Radio Clubs & Organizations](clubs.md)++[Projects](projects.md)++[JS8 mode info/help](js8help.md)
----------
If you’re looking for me on the air your best bet is via  [JS8](http://js8call.com/)  on 7.078mHz. My station stays on the air as near 24/7/365 as the weather and lightning allow. If there’s lighting in view on  [THIS MAP](http://www.lightningmaps.org/?lang=en#y=34.0379;x=-88.9081;z=9;t=2;m=oss;r=0;s=15;o=0;b=68.93;d=2;dl=2;dc=0;ts=0;tr=1;%5Dhttp://www.lightningmaps.org/?lang=en#y=34.0379;x=-88.9081;z=9;t=2;m=oss;r=0;s=15;o=0;b=68.93;d=2;dl=2;dc=0;ts=0;tr=1;)  theres a very good chance my station will be off.

Please leave a message and repeat until you receive an ack from my station please. Sometimes, for the various reasons that are ham radio, messages may not get thru the first time, or first several times.

[![JS8](https://i.postimg.cc/sxdKgfqk/web-header-2-1024x165.png)](http://js8call.com/)

#### [JS8Call Main page](http://js8call.com/)
<!--stackedit_data:
eyJoaXN0b3J5IjpbMTE4MTM5MTgxOSwxOTAzMTAwODk3LC00OT
I3MjgzMTJdfQ==
-->