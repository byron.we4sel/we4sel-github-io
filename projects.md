# Projects
### ++[Home](index.md) ++[Station Info](station.md)++[On The Air](ontheair.md)++[Online Radio Clubs & Organizations](clubs.md)++[Projects](projects.md)++[JS8 mode info/help](js8help.md)
---
Parking lot for projects in their various states. 

---

 - Waiting 
	 - 3018 mini CNC 
	 - Hydroponic/Aquaponic Garden
 - Started   
	 - 3 sisters gardens 
	 - MK4 VW 1.9L PD-TDI  
   
 - Finished 
	 - 50w CO<sup>2</sup> Laser Cutter/Engraver 
   
   
 - Stalled 
	 - Solar storage upgrade 
	 - ESP8266 MQTT thing 
	 - "Short Bus" to RV

---

  "Talking to Aliens" 
<!--stackedit_data:
eyJoaXN0b3J5IjpbLTgwMzI0NzEyNSwyMTQ3NTQwNzcsMTE3NT
I4NDc2NSw4MzI0ODgxMDgsMTMwMTMxNjcyMiw1NTAzMDMyOTQs
MTQyODg3MjQ0MSw4NjQwMjcyNTNdfQ==
-->