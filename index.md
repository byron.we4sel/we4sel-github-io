# Welcome to Byron's corner *aka WE4SEL
### ++[Home](index.md) ++[Station Info](station.md)++[On The Air](ontheair.md)++[Online Radio Clubs & Organizations](clubs.md)++[Projects](projects.md)++[JS8 mode info/help](js8help.md)

---
#### “If we could read the secret history of our enemies, we should find in each man’s life sorrow and suffering enough to disarm all hostility.” ― Henry Wadsworth Longfellow
So here you are. You  _are_  here, aren't you? If you’re looking for me on the air, your best bet is via  [JS8](http://js8call.com/)  on 7.078mHz.

(you  _are_  here!)

---

“Talking to Aliens” 

<!--stackedit_data:
eyJoaXN0b3J5IjpbLTExNzI1MTA2NzgsMTM2NzkyNDgzOCw0Mj
AwNzk1MTIsMTM3NjEzMzc5OSwtMTY3Mjk2MDcyOSwtMTY4MTI3
ODU5NV19
-->